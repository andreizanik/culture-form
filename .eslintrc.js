module.exports = {
    "extends": "airbnb",
    "rules": {
        "no-console": "off",
        "comma-dangle": "off",
        "quotes": "off",
        "react/prop-types": 0,
        "arrow-body-style": 0,
        "space-before-function-paren": 0,
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        "import/extensions": [1, "ignorePackages"],
        "no-underscore-dangle": 0,
        "react/button-has-type": 0,
        "jsx-a11y/label-has-associated-control": 0,
        "jsx-a11y/label-has-for": 0,
        "jsx-a11y/anchor-is-valid": 0
    },
    "env": {
        "browser": true
    },
    "parser": "babel-eslint"
};
