import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import {
  Formik,
  Form,
  Field,
  ErrorMessage
} from 'formik';
import reateAction from '../../utils/createAction.js';

const formSend = reateAction('FORM_SEND');

const validatorSchema = Yup.object().shape({
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  password: Yup.string()
    .min(8, 'Too Short!')
    .required('Required'),
});

const CultureForm = ({ dispatch }) => {
  const onSubmit = (values) => {
    dispatch(formSend(values));
  };

  return (
    <React.Fragment>
      <div className="col-7">
        <h1>Тестовая форма</h1>
      </div>

      <Formik
        initialValues={{ email: '', password: '' }}
        validationSchema={validatorSchema}
        onSubmit={onSubmit}
      >
        {() => (
          <Form className="col-7">
            <div className="form-group">
              <label htmlFor="email">Email address</label>
              <Field type="email" name="email" className="form-control" />
              <ErrorMessage
                name="email"
                render={msg => <small className="form-text text-muted">{msg}</small>}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Email address</label>
              <Field type="password" name="password" className="form-control" />
              <ErrorMessage
                name="password"
                render={msg => <small className="form-text text-muted">{msg}</small>}
              />
            </div>
            <div className="form-group">
              <button className="btn btn-primary" type="submit">
                Submit
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default connect()(CultureForm);
