import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import CultureForm from './components/cultureForm/cultureForm.js';
import reducers from './reducers/index.js';
import appSaga from './saga/index.js';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  reducers,
  {},
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

const App = () => {
  return (
    <Provider store={store}>
      <div className="container">
        <div className="row">
          <CultureForm />
        </div>
      </div>
    </Provider>
  );
};

sagaMiddleware.run(appSaga);

export default App;
