import axios from 'axios';

export default class {
  static sendForm(payload) {
    if (process.env.NODE_ENV === 'production') {
      return axios.post(`${window.API_URL}/initdash`, payload);
    }

    console.log('[DEVOLOPMENT API] -> payload:', payload);

    return {
      status: 200,
      payload: {
        data: { name: 'testName', role: 'user' }
      },
    };
  }
}
