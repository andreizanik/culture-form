import { combineReducers } from 'redux';
import form from './form.js';

export default combineReducers({
  form,
});
