import { isImmutable, fromJS } from 'immutable';

const initialState = {
  loading: false,
  data: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'FORM_SEND':
      return state.set('loading', true);

    case 'FORM_SUCCEEDED':
      return state
        .set('data', action.payload.data)
        .set('loading', false);

    default:
      return isImmutable(state) ? state : fromJS(state);
  }
};
