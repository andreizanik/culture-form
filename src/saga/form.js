import { call, put, takeEvery } from 'redux-saga/effects';
import Api from '../services/api';

function* sendForm(action) {
  try {
    const res = yield call(Api.sendForm, action.payload);
    yield put({ type: 'FORM_SUCCEEDED', payload: res.payload });
  } catch (e) {
    yield put({ type: 'FORM_FAILED', error: e });
  }
}

function* form() {
  yield takeEvery('FORM_SEND', sendForm);
}

export default form;
