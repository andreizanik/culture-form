import { all } from 'redux-saga/effects';
import form from './form.js';

export default function* appSaga() {
  yield all([
    form()
  ]);
}
